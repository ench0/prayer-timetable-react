const wordCount = (str: string) => str.split(' ').filter(n => n !== '').length

export default { wordCount }
export { wordCount }

/* eslint-env browser */
/* eslint-disable no-restricted-globals */
/* eslint-disable no-unused-vars */
/* eslint-disable no-restricted-syntax */

self.addEventListener('activate', e => {
  // @ts-ignore
  self.registration
    .unregister()
    // @ts-ignore
    .then(() => self.clients.matchAll())
    // @ts-ignore
    .then(clients => {
      // @ts-ignore
      clients.forEach(client => client.navigate(client.url))
    })
})

navigator.serviceWorker.getRegistrations().then(registrations => {
  for (const registration of registrations) {
    registration.unregister()
  }
})

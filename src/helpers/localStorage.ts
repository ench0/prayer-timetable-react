import localforage from 'localforage'

export const loadState = () => {
  let serialisedState

  localforage
    .getItem('count')
    .then(() => localforage.getItem('count'))
    .then(value => {
      // we got our value
      // console.log(value)
      serialisedState = value
      console.log('getting state...', serialisedState)
      return serialisedState
    })
    .catch(err => {
      // we got an error
      console.log(err)
    })

  // try {
  //   // const serialisedState = localforage.getItem('count')

  //   let serialisedState

  //   localforage
  //     .getItem('count')
  //     .then(() => localforage.getItem('count'))
  //     .then((value) => {
  //       // we got our value
  //       // console.log(value)
  //       serialisedState = value
  //       console.log('getting state...', serialisedState)
  //     })
  //     .catch((err) => {
  //       // we got an error
  //       console.log(err)
  //     })

  //   // localforage.getItem('jamaahShow'))
  //   // localforage.setItem('jamaahShow', data)
  //   // localforage.setItem('settings', newsettings)
  //   if (serialisedState === null) {
  //     return undefined
  //   }
  //   return serialisedState
  // } catch (error) {
  //   console.log('error', error)
  //   return undefined
  // }
}

export const saveState = (state: any) => {
  try {
    const serialisedState = state
    localforage.setItem('count', serialisedState).then(() => {
      console.log('setting state...', serialisedState)
    })

    // localforage.setItem('count', serialisedState).then(function () {
    //   return localforage.getItem('key');
    // }).then(function (value) {
    //   // we got our value
    // }).catch(function (err) {
    //   // we got an error
    // });
  } catch (error) {
    console.log('error', error)
  }
}

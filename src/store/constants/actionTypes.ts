export const GET_TIMETABLE: string = 'GET_TIMETABLE'
export const GET_TOGGLE: string = 'GET_TOGGLE'
export const GET_MASJID: string = 'GET_MASJID'

export const GET_UPDATE: string = 'GET_UPDATE'
export const GET_LOCAL: string = 'GET_LOCAL'

export const GET_PRAYERS: string = 'GET_PRAYERS'
export const GET_TIME: string = 'GET_TIME'

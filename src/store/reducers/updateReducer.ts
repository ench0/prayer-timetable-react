import { GET_UPDATE, GET_LOCAL } from '../constants/actionTypes'
import { initialState } from './initialState'

// IMPORTANT: Note that with Redux, state should NEVER be changed.
// State is considered immutable. Instead,
// create a copy of the state passed and set new values on the copy.
// Note that I'm using Object.assign to create a copy of current state
// and update values on the copy.
const updateReducer = (state = initialState.update, action: any) => {
  switch (action.type) {
    case GET_UPDATE:
      // console.log('UPDATEREDUCER', state, action)
      return {
        ...state,
        downloaded: action.payload.downloaded,
        success: action.payload.success,
      }
    case GET_LOCAL:
      // console.log('UPDATEREDUCER', state, action)
      return {
        ...state,
        downloaded: action.payload.downloaded,
      }
    default:
      return state
  }
}

export default updateReducer

// updSettings,
// updDownloaded,
// updSuccess,
// updRefreshFail,

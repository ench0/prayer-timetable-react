import { getUpdate } from './getUpdate'
import getTime from './getTime'
import getPrayers from './getPrayers'

let timer: any
let updatetimer: any

// RUN
const run = async () => {
  await getTime()
  getPrayers()
}
// TICK
const tick = async () => {
  await clearInterval(timer)
  timer = setInterval(() => run(), 1000)
}

const update = async (interval = 1) => {
  getUpdate()
  await clearInterval(updatetimer)
  updatetimer = setInterval(() => getUpdate(), 1000 * 60 * interval)
}

export { tick, update }

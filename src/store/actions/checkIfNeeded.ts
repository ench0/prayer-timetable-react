// @ts-ignore
import store from '../index'
import * as defsettings from '../../config/settings.json'
import { differenceInMinutes } from 'date-fns'

const { log } = store.getState().settings

const checkIfNeeded = (downloaded: any, success: boolean, updateInterval: number) => {
  /* eslint-env browser */

  // is online app?
  const condition1 = defsettings.online

  // online now?
  const condition2 = navigator.onLine

  // no updates?
  const condition3 = updateInterval !== 0

  // not downloaded recently?
  // console.log('downloaded', downloaded)
  // console.log('updateInterval', updateInterval)

  const diff = downloaded instanceof Date ? differenceInMinutes(new Date(), downloaded) : 0
  // console.log(diff)
  const condition4 = downloaded === {} || downloaded === null || diff >= updateInterval
  // console.log(condition4)
  // failed recently?
  const condition5 = !success

  log && console.log('conditions:', condition1, condition2, condition3, condition4, condition5)

  if (
    (condition1 && condition2 && condition3 && condition4) ||
    (condition1 && condition2 && condition3 && condition5)
  ) {
    log && console.log('update needed')
    return true
  }
  log && (condition2 ? console.log('update NOT needed') : console.log('update NOT possible'))
  return false
}

export default checkIfNeeded

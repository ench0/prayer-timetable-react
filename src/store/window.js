// EXPOSE store.getState() and others to browser console
import localforage from 'localforage'
import store from './index'
// import { addArticle } from './actions/index'
/* eslint-disable */
window.store = store
window.localforage = localforage
// window.addArticle = addArticle

/* ******************** */
/*        REDUX         */
/* ******************** */
// import index from './redux/index'
// store.subscribe(() => console.log('Look ma, Redux!!'))
// import { Provider } from 'react-redux'
// import { createStore } from 'redux'

// import { loadState, saveState } from './helpers/localStorage'
// const persistedState = loadState()
// console.log(persistedState)
// console.log(loadState())

// const initialState = {
//   count: 0,
// }
// function reducer(state = initialState, action) {
//   switch (action.type) {
//     case 'INCREMENT':
//       return {
//         count: state.count + 1,
//       }
//     case 'DECREMENT':
//       return {
//         count: state.count - 1,
//       }
//     case 'SET':
//       return {
//         count: state.count,
//       }
//     default:
//       return state
//   }
// }
// const store = createStore(reducer)

// store.subscribe(() => {
//   saveState(store.getState())
// })
// console.log(store.getState())
// END REDUX STUFF
/* ******************** */

/* eslint-disable */

import React, { Component } from 'react'

import { connect } from 'react-redux'

// import Head from './components/Head'
import Overlay from './components/Overlay'
import Header from './components/Header'
import Clock from './components/Clock'
import Prayers from './components/Prayers'
import Countdown from './components/Countdown'
import Message from './components/Message'
import Footer from './components/Footer'

import styles from './styles'

// import { tick, update } from './helpers/update'

const { AppStyle, GlobalStyle } = styles

const TimetableApp = () => (
  <AppStyle>
    {/* <Head /> */}
    <Header />
    <Clock />
    <Prayers />
    <Countdown />
    <Message />
    <Footer />
    {/* <Overlay /> */}
    <GlobalStyle />
  </AppStyle>
)

const ConnectedTimetableApp = connect(
  null,
  // mapDispatchToProps
)(TimetableApp)

export default ConnectedTimetableApp
export { TimetableApp }

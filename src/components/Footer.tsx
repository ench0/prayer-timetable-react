import * as React from 'react'

import { connect } from 'react-redux'
import { formatDistance } from 'date-fns' // setDay, setHours, setMinutes
import { appendZero, hijriConvert } from '../helpers/func'

// @ts-ignore
import { Offline, Online } from 'react-detect-offline'
// @ts-ignore
import Refresh from 'react-feather/dist/icons/refresh-cw'
// @ts-ignore
import Download from 'react-feather/dist/icons/download'
import styles from '../styles'
import { State } from '../store/reducers/initialState'

import { wifiOn, wifiOff } from '../styles/img/icons'

type StateProps = Pick<State, 'day' | 'settings' | 'update'>

const FooterStyle = styles.footer

const updatedDisp = (settings: any) => {
  let result
  if (!settings.online) {
    result = null
  } else if (settings.updated < 1514764800) {
    result = (
      <div className="right">
        <Refresh size={24} />
        {' - '}
      </div>
    )
  } else {
    result = (
      <div className="right">
        <Refresh size={24} />
        {formatDistance(new Date(settings.updated * 1000), new Date(), { includeSeconds: true })} ago
      </div>
    )
  }
  return result
}

// RAMADAN
const ramadanDisp = (day: any) => {
  const result = day.ramadanCountdown ? <div className="left">{`${day.ramadanCountdown} to Ramadan`}</div> : null
  return result
}

// JUMMUAH
const jummuahDisp = (settings: any) => {
  // let friday = setDay(new Date(), 5, { weekStartsOn: 1 }).setMinutes(settings.jummuahtime[1])
  // friday = setHours(new Date(friday), settings.jummuahtime[0])

  // const jummuahTime = format(new Date(friday), 'HH:mm') // DD/MM/ddd

  const result = settings.jummuahtime[0] ? (
    <div>{`Jummuah ${settings.jummuahtime[0]}:${appendZero(settings.jummuahtime[1])}`}</div>
  ) : (
    0
  )
  return result
}

// TARAWEEH
const taraweehDisp = (settings: any) => {
  const now = new Date()

  const hijri = hijriConvert(now)
  const result =
    hijri.hm === 9 ? (
      <div>{`Taraweeh ${settings.taraweehtime[0]}:${appendZero(settings.taraweehtime[1])}`}</div>
    ) : null

  return result
}

// ONLINE
const onlineDisp = (settings: any) => {
  const result = settings.online ? (
    <div className="wifi">
      <Offline>{wifiOff}</Offline>
      <Online>{wifiOn}</Online>
    </div>
  ) : null
  return result
}

// REFRESHDIFF
const refreshDiffDisp = (settings: any, update: any) => {
  /* eslint-disable babel/no-unused-expressions */
  let diff
  update.downloaded !== '-' && update.downloaded instanceof Date
    ? (diff = `${formatDistance(new Date(update.downloaded), new Date(), { includeSeconds: true })} ago
    `)
    : (diff = '-')
  const result = settings.online ? (
    <div className="right">
      <Download size={24} />
      {diff}
    </div>
  ) : null

  return result
}

// MAPPING
const mapStateToProps = (state: StateProps) => ({
  day: state.day,
  settings: state.settings,
  update: state.update,
})

// COMPONENT
const ConnectedFooter = ({ day, settings, update }: StateProps) => (
  <FooterStyle>
    {onlineDisp(settings)}
    {jummuahDisp(settings)}
    {ramadanDisp(day)}
    {taraweehDisp(settings)}
    <div className="info">
      {refreshDiffDisp(settings, update)}
      {updatedDisp(settings)}
    </div>
  </FooterStyle>
)

// EXPORT
const Footer = connect(mapStateToProps)(ConnectedFooter)
export default Footer

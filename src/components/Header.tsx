import * as React from 'react'

import { connect } from 'react-redux'
import { format } from 'date-fns'
import styles from '../styles'
// @ts-ignore
import logo from '../config/logo.svg'
import { hijriDate } from '../helpers/func'

import { State } from '../store/reducers/initialState'
type StateProps = Pick<State, 'prayers'>

const HeaderStyle = styles.header

const logoImg = <img src={logo} alt="logo" />

const mapStateToProps = (state: StateProps) => ({ prayers: state.prayers })

const ConnectedHeader = ({ prayers }: StateProps) => (
  <HeaderStyle>
    <div>{format(prayers.now, 'EEE d MMMM yyyy')}</div>
    <div className="logo">{logoImg}</div>
    <div>{hijriDate(prayers.hijri)}</div>
    {/* <div>{prayers.hijri.format('iDD iMMMM iYYYY')}</div> */}
  </HeaderStyle>
)
const Header = connect(mapStateToProps)(ConnectedHeader)
export default Header

import * as React from 'react'

import { connect } from 'react-redux'

import styles from '../styles'

import { State } from '../store/reducers/initialState'

type StateProps = Pick<State, 'settings'>

const MessageStyle = styles.message

const mapStateToProps = (state: StateProps) => ({ settings: state.settings })

const ConnectedMessage = ({ settings }: StateProps) => (
  <MessageStyle>
    {settings.announcement ? <h3>{settings.announcement}</h3> : ''}
    {settings.text.ar ? <div className="ar">{settings.text.ar}</div> : null}
    {settings.text.en ? <div className="en">{settings.text.en}</div> : null}
  </MessageStyle>
)
const Message = connect(mapStateToProps)(ConnectedMessage)
export default Message

import * as React from 'react'

import { connect } from 'react-redux'

import styles from '../styles'
// @ts-ignore
import logo from '../config/logo.svg'

import { State } from '../store/reducers/initialState'

type StateProps = Pick<State, 'day' | 'settings'>

const OverlayStyle = styles.overlay

const logoImg = <img src={logo} alt="logo" />

const mapStateToProps = (state: StateProps) => ({ day: state.day, settings: state.settings })

const ConnectedOverlay = ({ day, settings }: StateProps) => {
  const output = day.overlayActive ? (
    <OverlayStyle>
      <div className="logo">{logoImg}</div>
      <div>
        <h1>{day.overlayTitle}</h1>
        {/* <div>{day.now.format('ddd D MMMM YYYY')}</div> */}
        {/* <div>{day.hijri.format('iD iMMMM iYYYY')}</div> */}
      </div>
      <div className="title">{settings.title}</div>
    </OverlayStyle>
  ) : null
  return output
}
const Overlay = connect(mapStateToProps)(ConnectedOverlay)
export default Overlay

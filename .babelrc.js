module.exports = {
  presets: [
    [
      '@babel/preset-env',
      {
        targets: {
          node: 'current',
        },
        // modules: false,
      },
    ],
    '@babel/react',
  ],
  plugins: [
    '@babel/plugin-transform-runtime',
    'styled-components',
    // '@babel/react',
    // [
    //   '@babel/plugin-transform-runtime',
    //   {
    //     // polyfill: false,
    //     regenerator: true,
    //   },
    // ],
  ],
  ignore: ['builtins'],
}
